<?php
require_once("./class.php");
header('Content-Type: application/json');
$u_s = new CRUD("user-services", "milogy", ["id", "userid", "serviceid", "status", "start", "finish"]);
$u_s->setForeignkey([["key" => "userid", "table" => "users"], ["key" => "serviceid", "table" => "services"]]);
$state = $u_s->getErrorConnection();
if (isset($state)) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
    echo json_encode($state);
    die();
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $user = new CRUD("users", "milogy", ["userid", "username", "firstname", "lastname"], ["username"]);
    $service = new CRUD("services", "milogy", ["serviceid", "title", "description", "orderUser", "price"]);

    $data_json = file_get_contents('php://input');
    $data = json_decode($data_json, true);
    if (!isset($data["userid"]) && !isset($data["serviceid"])) {

        echo json_encode(["error code" => 422, "error message" => "STATE[422]: Some fields are not entered"]);
        die;
    }
    if (!$user->readRow(["userid" => $data["userid"]])) {
        $id = $data["userid"];
        header("HTTP/1.0 404 not found");
        echo json_encode([
            "error-code" => 404,
            "error-message" => "STATE[404]:$id Not found"
        ]);
    }
    if (!$service->readRow(["serviceid" => $data["serviceid"]])) {
        header("HTTP/1.0 404 not found");
        $id = $data['serviceid'];
        echo json_encode([
            "error-code" => 404,
            "error-message" => "STATE[404]:$id Not found"
        ]);
    }
    $userid = $data['userid'];
    $serviceid = $data["serviceid"];
    $res = $u_s->readRow(["`userid`" => $userid, "`serviceid`" => $serviceid], "AND");
    if ($res) {
        header("HTTP/1.0 422 input error");
        echo json_encode(["error code" => 422, "error message" => "STATE[422]: The request has already been sent"]);
    } else {
        date_default_timezone_set('Asia/Tehran');
        $start = date("Y-m-d");
        $res = $u_s->create(["userid" => $userid, "serviceid" => $serviceid, "status" => 0, "start" => $start]);
        if ($res) {
            header("HTTP/1.0 201 Creted");
            echo json_encode($res);
        } else {
            $state = $user->getErrorConnection();
            header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
            echo json_encode($state);
        }
    }
}
if ($_SERVER["REQUEST_METHOD"] == "GET") {
    $res = $u_s->readRow();
    if (sizeof($res) >= 1)
        echo json_encode($res, true);
    else {
        $err = $user->getErrorConnection();
        header("HTTP/1.0 404 Not Found");
        echo json_encode($err);
    }
}
if ($_SERVER["REQUEST_METHOD"] == "PUT") {

    $id = $_GET["id"];
    // $userid = $_GET['userid'];
    // $serviceid = $_GET["serviceid"];
    if (!isset($id)) {
        echo json_encode(["error code" => 422, "error message" => "STATE[422]: Some fields are not entered"]);
        die;
    }
    $row = $u_s->readRow(["id" => $id]);
    $row = $row[0];
    if ($row && $row["status"] == false) {
        date_default_timezone_set('Asia/Tehran');
        $finish = date("Y-m-d");
        $up = $u_s->update(["id" => $id], [
            "userid" => $row["userid"], "serviceid" => $row["serviceid"], "status" => 1, "start" => $row["start"], "finish" => $finish
        ]);

        if ($up)
            echo json_encode($u_s->readRow(["id" => $id]));
        else {
            $err = $user->getErrorConnection();
            if ($err["error-code"] == 404)
                header("HTTP/1.0 404 Not Found");
            else
                header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
            echo json_encode($err);
            die();
        }
    } elseif ($row == false) {
        $err = $user->getErrorConnection();
        if ($err["error-code"] == 404)
            header("HTTP/1.0 404 Not Found");
        else
            header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
        echo json_encode($err);
        die();
    } elseif ($row["status"]) {
        header("HTTP/1.0 422 input error");
        echo json_encode(["error code" => 422, "error message" => "STATE[422]: This service was already finished"]);
    }
}
