-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 26, 2020 at 01:18 PM
-- Server version: 10.5.3-MariaDB-1:10.5.3+maria~focal
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `milogy`
--

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `serviceid` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` varchar(400) NOT NULL,
  `orderUser` varchar(200) NOT NULL,
  `price` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`serviceid`, `title`, `description`, `orderUser`, `price`) VALUES
(1, 'carpet', 'ccccccccccccc', 'ooooooooooo', 11112222),
(2, 'carpet1', 'ccccccccccccc1', 'ooooooooooo1', 11111111),
(20, 'ttttt', 'dddddddddd', 'uuooooou', 777777),
(27, 'title', 'description', 'orderUser', 9000000),
(28, 'title44', 'description66', 'orderUser55', 89890000),
(29, 'title44', 'description66', 'orderUser55', 89890000),
(30, 'title44', 'description66', 'orderUser55', 89890000),
(34, 'ttt34', 'dddd34', 'ooooo34', 34343434);

-- --------------------------------------------------------

--
-- Table structure for table `user-services`
--

CREATE TABLE `user-services` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `serviceid` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `start` date NOT NULL,
  `finish` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user-services`
--

INSERT INTO `user-services` (`id`, `userid`, `serviceid`, `status`, `start`, `finish`) VALUES
(1, 142, 1, 0, '2020-06-26', NULL),
(2, 142, 1, 0, '2020-06-26', NULL),
(3, 15, 20, 1, '2020-06-26', '2020-06-26'),
(4, 4, 20, 0, '2020-06-26', NULL),
(5, 6, 20, 0, '2020-06-26', NULL),
(6, 32, 20, 0, '2020-06-26', NULL),
(7, 12, 20, 0, '2020-06-26', NULL),
(8, 13, 20, 1, '2020-06-26', '2020-06-26'),
(11, 15, 34, 0, '2020-06-26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userid` int(11) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(300) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `username`, `password`, `firstname`, `lastname`) VALUES
(4, 'sara_ph', '$2y$10$P.paxUJ93byfwxbp2agntuT.bNOQX6y/.jJpAwq3j7mh5BoDp2vbS', 'sara', 'fathpour'),
(6, 'ali6', '$2y$10$wKx.YNdgeCN.yxReW8W8Bej1lMhad/pg1ECFYsqnaZuQZJAqoSR1W', 'ali', 'kermani'),
(7, 'maneli', '$2y$10$nhZMw2cpjvpY95mxhegi0OklKSorK88ZMjK/zeVhnZaIFr2zIVXp.', 'maneli', 'fatemi'),
(8, 'sina.fapo', '$2y$10$dAI5PVnNXvtEfpSf4QWnX.abEKzrge62nHO5B/g03ltBwI7b8EAI6', 'sina', 'fathpour'),
(12, 'samira_b', '$2y$10$cMjzfLz.NPewCxdErTlsWOQ0ECQwoICGnetxenw5Es5Gml3PZSw7C', 'samira', 'baghban'),
(13, 'maneli1', '$2y$10$JJ2.f4di5spZaYA6uow9N.Vqk85F6724fXlZI3z3ktw6GQnVKa0ly', 'samira1', 'baghban1'),
(15, '155555', '$2y$10$EChLTLQgexLcqxuhIIXof.basEx13N3S/rddeafHxzabqF2zhBari', 'dd', 'rr'),
(16, 'ee', '$2y$10$vVUEuc9wuoE7CabQaxvdreWQjxG2GnTxfh1BMsmaj7CvMngaqvAAm', 'dd', 'rr'),
(17, 'ee33', '$2y$10$jHwRTDfnO3p.rWINqq8y9OtOeqSo3cEOwC0bDqKLCxQC5lAxNGRTm', 'dd', 'rr'),
(22, 'iioo11', '$2y$10$gvfUbvB4Y1LUg2hpruZ9HOaT.azsTt1drwqlYqU/wBsiZlNENNvai', 'dd', 'rr'),
(29, 'ee33dd', '$2y$10$W0VN5izCsCNjCjhaSyiFK.42nl53mRIqq/8.Vjie1bs5YWhzhUuEa', 'dd', 'rr'),
(32, '77779', '$2y$10$KbZt5Ps0UdQv.A/LKoC4ReaAoQlmOLwSkN5yS8jdPRhFI/sfVx2qG', 'dd', 'rr'),
(33, 'uuuu99uu', '$2y$10$4ZRs8.rGmhNFSzHIG3XrTuN9j8oZnXj3k0IyIMiyhGUAwSOEWnrg.', 'dd', 'rr'),
(34, 'ueeuuu99uu', '$2y$10$pi0iWMeoFNaWo0/96LzZgeaUaqPZNLxLDIsEij5ezmsr4XfwNofO2', 'dd', 'rr'),
(136, '78797', '$2y$10$VRfmiNiBYhfFQoUlYerZB.xTG9VtKXGyrrRoZYzgxUh/EmfDDQDfu', 'uuu', 'yyyyuuu'),
(137, 'yuyiuyui', '$2y$10$vKKtvqfi37dcTMd7CHHCCeTIaKk3d2M1VwyHBoWBKR28YbryOC5zm', 'sara', 'fathpour'),
(138, 'yuyiouyui', '$2y$10$EVZ0Q5GlFqidFgRz0UhATuZvj0BEYur340ox9NtzPJbzEvOeE7Yse', 'sara', 'fathpour'),
(139, 'yuyiouyui88', '$2y$10$iQXB2.CRQ.OvoxWzLaNc3OK560YHE/jTvpCvxBhQmrFHV1apbUobi', 'sara', 'fathpour'),
(141, 'hjkhj', '$2y$10$/QqbC35Rngla5sL6Cm8.NuNtYNd9Acz.tpGFSXlWes7K9wM17J.WC', 'uuu', 'yyyyuuu'),
(142, 'ph', '$2y$10$3ntgB1/hdAzB/JHTRRe7tOGfDK5tk/5alT/IIGnWebL77QJ4GVVDm', 'uuu', 'yyyyuuu'),
(145, '88', '$2y$10$01BjVO4ZAnDzN/r1zUNWDOZGsWnP8eBedku3.zjaHobeSfFnuruGK', 'dd', 'rr');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`serviceid`);

--
-- Indexes for table `user-services`
--
ALTER TABLE `user-services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `serviceid` (`serviceid`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userid`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `serviceid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `user-services`
--
ALTER TABLE `user-services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user-services`
--
ALTER TABLE `user-services`
  ADD CONSTRAINT `serviceid` FOREIGN KEY (`serviceid`) REFERENCES `services` (`serviceid`),
  ADD CONSTRAINT `userid` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
