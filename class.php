<?php
class CRUD
{
    private $conn, $tbl, $allowFields, $error_connecttion = null, $unique_fields = null, $foreign_key=null;
    public function __construct($tableName, $dbName, array $allow=null, array $unique = null)
    {
        if (isset($unique))
            $this->setUnique($unique);
        if(isset($allow))
            $this->setFields($allow);
        $this->setTable($tableName);
        $this->connectDb("localhost", $dbName, "root", "1377");
    }
    public function connectDb(string $host, string $db, string $user, string $pass, string $options = null)
    {
        if (!isset($options)) {
            $options = [
                PDO::ATTR_EMULATE_PREPARES   => false, // turn off emulation mode for "real" prepared statements
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, //turn on errors in the form of exceptions
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, //make the default fetch be an associative array
            ];
        }
        $dsn = "mysql:host=$host;dbname=$db; charset=utf8mb4";
        set_exception_handler(function ($e) {
            $this->setErrorConnection(["error-code" => 500, "error-message" => $e->getMessage()]);
        });
        try {
            $conn = new PDO($dsn, $user, $pass, $options);
            $this->conn = $conn;
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int) $e->getCode());
        }
    }
    public function setUnique($unique)
    {
        $this->unique_fields = $unique;
    }
    public function getUnique()
    {
        return $this->unique_fields;
    }
    private function setErrorConnection($error)
    {
        $this->error_connecttion = $error;
    }
    public function getErrorConnection()
    {
        return $this->error_connecttion;
    }
    public function setTable(string $tbl)
    {
        $this->tbl = "`$tbl`";
    }
    public function setFields(array $fields)
    {
        array_walk($fields, function (&$value, $key) {
            $value = "`$value`";
        });
        $this->allowFields = $fields;
    }
     /**
       * @param array $foreign [
       *                            0 => ["key"=>$foreign["key"], "table"=> $foreign["table"]]
       *                       ] 
       */
    public function setForeignkey(array $foreign)
    {
        foreach ($foreign as $value) {
            $this->foreign_key[]=["keyName"=>$value["key"], "tblName"=> $value["table"]];
        }
    }
    private function existUniqueField($values)
    {
        $cond = [];
        foreach ($values as $key => $value) {
            if (in_array($key, $this->unique_fields))
                $cond[$key] = $value;
        }
        if (!$exist_field = $this->readRow($cond, 'OR'))
            return false;
        $cond_values = implode(",", $cond);
        $cond_keys = implode(",", array_keys($cond));
        $err_mess = "SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry '$cond_values' for keys '$cond_keys'";
        $this->setErrorConnection([
            "error-code" => 500,
            "error-message" => $err_mess
        ]);
        return true;
    }
    // private function existForeignkey(array $values) 
    // {
    //     foreach ($this->foreign_key as $key => $value) {
    //     }
    // }
    ////////* CRUD:
    ////* Create 
    public function create(array $values)
    {
        $unique = $this->unique_fields;
        if ($unique != null) {
            if ($this->existUniqueField($values))
                return false;
            if (($this->readRow([$unique[0] => $values[$unique[0]]]))) {
                $this->setErrorConnection([
                    "error-code" => 500,
                    "error-message" => "$unique[0] has already been registered"
                ]);
                return false;
            }
        }
        // if($this->foreign_key !=null)
        // {

        // }
        $fields = array_keys($values);
        $fields = implode(", ", $fields);
        ////*
        $param = array_values($values);
        ////*
        $num = count($values);
        $place = str_repeat('?, ', $num - 1) . "?";
        ////*
        $query = "INSERT INTO $this->tbl ($fields) VALUES ($place)";
        $stmt = $this->conn->prepare($query);
        if ($stmt->execute($param)) {
            $id = $this->allowFields[0];
            $q = $this->conn->query("SELECT $id FROM $this->tbl ORDER BY $id DESC LIMIT 1");
            $row = $q->fetchColumn();
            return $this->readRow([$this->allowFields[0] => $row]);
        } else return false;
    }
    ////* Read
    public function readRow($cond = null, $operator = 'AND')
    {
        $allow = implode(", ", $this->allowFields);
        if (!isset($cond)) {
            $query = "SELECT $allow FROM $this->tbl";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
        } else {
            $values_cond = array_values($cond);
            $keys_cond = array_keys($cond);
            array_walk($keys_cond, function (&$value, $key) {
                $value .= ' = ?';
            });
            $keys_cond = count($cond) > 1 ? implode(" ".$operator." ", $keys_cond) : $keys_cond[0]; //! $operator = OR or AND
            $query = "SELECT $allow FROM $this->tbl WHERE $keys_cond";
            $stmt = $this->conn->prepare($query);
            $stmt->execute($values_cond);
        }
        $data = [];
        while ($res = $stmt->fetch())
            $data[] = $res;

        if (sizeof($data) == 0) {
            $this->setErrorConnection([
                "error-code" => 404,
                "error-message" => "STATE[404]: Not found"
            ]);
            return false;
        }
        return $data;
    }

    ////* Update
    public function update(array $cond, array $newVal)
    {
        
        if (!$this->readRow($cond))
            return false;
        if (sizeof($this->unique_fields) > 0)
            if ($this->existUniqueField($newVal))
                return false;
        //
        $keys_cond = array_keys($cond);
        $values_cond = array_values($cond);
        ///
        array_walk($keys_cond, function (&$value, $key) {
            $value .= ' = ?';
        }); ////! append '=?' to array values
        $fields_cond = count($keys_cond) > 1 ? implode(" AND ", $keys_cond) : $keys_cond[0];
        ////*
        $values_newVal = array_values($newVal);
        //////! append '=?' to array values
        $fields_newVal = implode(" = ?, ", array_keys($newVal)) . " = ?";
        ////!
        
        $param = array_merge($values_newVal, $values_cond); ////! merge values
        $query = "UPDATE $this->tbl SET $fields_newVal WHERE $fields_cond";
        $stmt = $this->conn->prepare($query);
        if ($stmt->execute($param)) {
            return $this->readRow($cond);
        } else return false;
    }
    ////* Delete
    public function delete(array $cond)
    {
        $row = $this->readRow($cond);
        if (!$row)
            return false;
        $values_cond = array_values($cond);
        $keys_cond = array_keys($cond);
        array_walk($keys_cond, function (&$value, $key) {
            $value .= ' = ?';
        }); ////! append '=?' to array values
        if (count($keys_cond) > 1)
            $fields_cond = implode(" AND ", $keys_cond);
        else
            $fields_cond = $keys_cond[0];
        $query = "DELETE FROM $this->tbl WHERE $fields_cond";
        $stmt = $this->conn->prepare($query);
        if ($stmt->execute($values_cond))
            return $row;
        return false;
    }
}
